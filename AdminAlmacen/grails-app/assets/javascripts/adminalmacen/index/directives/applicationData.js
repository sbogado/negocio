//= wrapped

angular
    .module("adminalmacen.index")
    .directive("applicationData", applicationData);

function applicationData() {
    var directive = {
        restrict: "E",
        templateUrl: "/adminalmacen/index/applicationData.html",
        controller: "IndexController",
        controllerAs: "vm",
        transclude: true,
        scope: {},
        bindToController: {
        }
    };

    return directive;
}
