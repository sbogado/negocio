//= wrapped
//= require_self
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree templates
//= require routes
angular.module("adminalmacen.index", ["adminalmacen.core"]);
