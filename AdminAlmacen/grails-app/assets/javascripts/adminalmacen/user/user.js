//= wrapped
//= require /angular/angular 
//= require /adminalmacen/core/core
//= require_self
//= require routes
//= require_tree services
//= require_tree controllers
//= require_tree directives
//= require_tree domain
//= require_tree templates

angular.module("adminalmacen.user", ["adminalmacen.core"]);

