//= wrapped

angular.module("adminalmacen.user")
    .config(function($routeProvider) {
        $routeProvider.
        when('/user', {
            templateUrl: "/adminalmacen/user/user.html",
            controller: "UserListController as ctrl"
        }).
        when('/user/create', {
            templateUrl: "/adminalmacen/user/create.html",
            controller: "UserCreateController as ctrl"
        }).
        when('/user/edit/:userId', {
            templateUrl: "/adminalmacen/user/create.html",
            controller: "UserCreateController as ctrl"
        }).
        when('/user/:userId', {
            templateUrl: "/adminalmacen/user/show.html",
            controller: "UserShowController as ctrl"
        })
    });