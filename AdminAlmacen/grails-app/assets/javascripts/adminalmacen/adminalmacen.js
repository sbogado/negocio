//= wrapped
//= require /angular/angular
//= require /adminalmacen/core/core
//= require /adminalmacen/index/index
//= require /adminalmacen/user/user

angular.module("adminalmacen", [
    "ngRoute",
    "adminalmacen.core",
    "adminalmacen.index",
    "adminalmacen.user"
]);
