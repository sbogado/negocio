package ar.com.nsu.adminAlmacen

class TipoProducto {

	Long id;
	String tipoDeProducto;
	String descripcion;

    static constraints = {
    	tipoDeProducto unique:true
    }


    String toString(){
    	return this.tipoDeProducto;
    }

}
