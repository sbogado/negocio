package ar.com.nsu.adminAlmacen

class Producto {

	Long id;
	String nombreProduco;
	Long precioEstablecido;
	String codigoDelProducto;
	TipoProducto tipoDeProducto;
    

    static constraints = {
    	codigoDelProducto unique:true
    }

    String toString (){
    	return "Nombre: "+this.nombreProduco + "precio: " + precioEstablecido;
    }

}
